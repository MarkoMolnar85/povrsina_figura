﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Piramida : Kvadrat
    {
        double h;
        double a1;

        public double A1
        {
            get { return a1; }
            set { a1 = value; }
        }

        public double H
        {
            get { return h; }
            set { h = value; }
        }
        public Piramida(double a, double fh) 
            :base(a)
        {
            h = fh;
        }
        public Piramida(double a, double fa1, double fh)          //bitan redosled
            : base(a)
        {
            a1 = fa1; h = fh;
        }
        public override double Povrsina()
        {
            return base.Povrsina();
        }
        private double a1Kvadrat()
        {
            return a1 * a1;
        }
        public double PovrsinaTrostranePiramide()
        {
            double rezultat = 0;
            rezultat = (Povrsina() * Math.Sqrt(3)) / 4 + 3 * (A * h) /2;
            return rezultat;
        }
        public double PovrsinaPravilnaCetvorostranaPiramida()
        {
            return Povrsina() + 2 * A * h;
        }
        public double PovrsinaPravilnaSestostranaPiramida()
        {
            double rezultat = 0;
            rezultat = 6 * ((Povrsina() * Math.Sqrt(3)) / 4) + 6 * ((A * h) / 2);
            return rezultat;
        }
        public double PovrsinaTrostranaZarubljena()
        {
            double rezultat = 0;
            rezultat = (Povrsina() * Math.Sqrt(3)) / 4 + (a1Kvadrat() * Math.Sqrt(3)) / 4 + ( 3 * (A + a1) / 2) * h;
            return rezultat;
        }
        public double PovrsinaCetvorostranaZarubljena()
        {
            return Povrsina() + a1Kvadrat() + 2 * (A + a1) * h;
        }
        

    }
}

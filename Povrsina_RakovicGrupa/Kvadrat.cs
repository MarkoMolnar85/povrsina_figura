﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Kvadrat //osnova za kvadrate sve
    {
        private double _a;

        public double A
        {
            get { return _a; }
            set { _a = value; }
        }
        public Kvadrat(double a)
        {
            _a = a;
        }
        virtual public double Povrsina()       ///////
        {
            return _a * _a;
        }
    }
}

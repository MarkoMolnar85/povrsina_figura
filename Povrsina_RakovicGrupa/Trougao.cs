﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Trougao : Pravugaonik
    {
        double _c;

        public double C
        {
            get { return _c; }
            set { _c = value; }
        }
        double _s;

        public double S
        {
            get { return _s; }
            set { _s = value; }
        }
        public Trougao(double fa, double fb, double fc)
            : base(fa, fb)
        {
            _c = fc; _s = (A + B + _c) / 2;
        }
        virtual public double PovrsinaTrougla()
        {
            return Math.Sqrt(_s * (_s - A) * (_s - B) * (_s - _c));
        }
    }
}

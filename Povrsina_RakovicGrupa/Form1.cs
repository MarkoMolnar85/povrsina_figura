﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Povrsina_RakovicGrupa
{
    public partial class frmGlavnaForma : Form
    {
        string putanja = Directory.GetCurrentDirectory() ;
        public frmGlavnaForma()
        {
            InitializeComponent();
            cmbIzbor.Items.Add("Kvadrat");
            cmbIzbor.Items.Add("Pravugaonik");
            cmbIzbor.Items.Add("Trougao");
            cmbIzbor.Items.Add("Kocka");
            cmbIzbor.Items.Add("Lopta");
            cmbIzbor.Items.Add("Pravilna trostrana piramida"); //
            cmbIzbor.Items.Add("Pravilna cetvorostrana"); //
            cmbIzbor.Items.Add("Pravilna sestostrana piramida");//
            cmbIzbor.Items.Add("Trostrana zarubljena piramida");//
            cmbIzbor.Items.Add("Cetvorostrana zarubljena piramida");
            cmbIzbor.Items.Add("Kupa");
            cmbIzbor.Items.Add("Zarubljena kupa");
            cmbIzbor.Items.Add("Valjak");

            lblPrvi.Visible = lblDrugi.Visible = lblCetvrti.Visible = lblTreci.Visible = false;
            txt1.Visible = txt2.Visible = txt3.Visible = txt4.Visible = false;
            lblRezultat.Text = "Povrsina je: ";
            
        }

        private void cmbIzbor_SelectedIndexChanged(object sender, EventArgs e)
        {
            string izb = cmbIzbor.Text;
            switch (izb)
            {
                case "Kvadrat": lblPrvi.Visible = true; lblDrugi.Visible = lblTreci.Visible = lblCetvrti.Visible = false; //samo 1
                    txt1.Visible = true; txt2.Visible = txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    break;
                case "Pravugaonik": lblPrvi.Visible = lblDrugi.Visible = true; lblTreci.Visible = lblCetvrti.Visible = false;  //1 i 2
                    txt1.Visible = txt2.Visible = true; txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti b";
                    break;
                case "Trougao": lblPrvi.Visible = lblDrugi.Visible = lblTreci.Visible = true; lblCetvrti.Visible = false;  // 1 i 2 i 3
                    txt1.Visible = txt2.Visible = txt3.Visible = true; txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti b";
                    lblTreci.Text = "Uneti c";
                    break;
                case "Kocka": lblPrvi.Visible = true ;lblDrugi.Visible = lblCetvrti.Visible = lblTreci.Visible = false;
                    txt1.Visible = true; txt2.Visible = txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    break;
                case "Lopta": lblPrvi.Visible = true; lblDrugi.Visible = lblTreci.Visible = lblCetvrti.Visible = false;
                    txt1.Visible = true; txt2.Visible = txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti r";
                    break;
                case "Pravilna trostrana piramida": lblPrvi.Visible = lblDrugi.Visible = true; lblTreci.Visible = lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = true; txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti h";
                    break;
                case "Pravilna cetvorostrana": lblPrvi.Visible = lblDrugi.Visible = true; lblTreci.Visible = lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = true; txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti h";
                    break;
                case "Pravilna sestostrana piramida": lblPrvi.Visible = lblDrugi.Visible = true; lblTreci.Visible = lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = true; txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti h";
                    break;
                case "Trostrana zarubljena piramida": lblPrvi.Visible = lblDrugi.Visible = lblTreci.Visible = true; lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = txt3.Visible = true; txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti a1";
                    lblTreci.Text = "Uneti h";
                    break;
                case "Cetvorostrana zarubljena piramida": lblPrvi.Visible = lblDrugi.Visible = lblTreci.Visible = true; lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = txt3.Visible = true; txt4.Visible = false;
                    lblPrvi.Text = "Uneti a";
                    lblDrugi.Text = "Uneti a1";
                    lblTreci.Text = "Uneti h";
                    break;
                case "Zarubljena kupa": lblPrvi.Visible = lblDrugi.Visible = lblTreci.Visible = true; lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = txt3.Visible = true; txt4.Visible = false;
                    lblPrvi.Text = "Uneti r";
                    lblDrugi.Text = "Uneti R";
                    lblTreci.Text = "Uneti s";
                    break;
                case "Kupa": lblPrvi.Visible = lblDrugi.Visible = lblTreci.Visible = true; lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = txt3.Visible = true; txt4.Visible = false;
                    lblPrvi.Text = "Uneti r";
                    lblDrugi.Text = "Uneti R";
                    lblTreci.Text = "Uneti s";
                    break;
                case "Valjak": lblPrvi.Visible = lblDrugi.Visible = true; lblTreci.Visible = lblCetvrti.Visible = false;
                    txt1.Visible = txt2.Visible = true; txt3.Visible = txt4.Visible = false;
                    lblPrvi.Text = "Uneti r";
                    lblDrugi.Text = "Uneti h";
                    break;
                    
            }
            imgSlike.Image = Image.FromFile(putanja + @"\" + "slike program" + @"\" + cmbIzbor.Text + ".jpg"); 
            
        
        }

        private void btnPovrsina_Click(object sender, EventArgs e)
        {
            double izlaz = 0;
            string izb = cmbIzbor.Text;
            double jedan = 0;
            double dva = 0;
            double tri = 0;
            double cetri = 0;
            if (txt1.Text != null && txt1.Visible == true)
            {
                jedan = double.Parse(txt1.Text);
            }
            if (txt2.Text != null && txt2.Visible == true)
            {
                dva = double.Parse(txt2.Text);
            }
            if (txt3.Text != null && txt3.Visible == true)
            {
                tri = double.Parse(txt3.Text);
            }
            if (txt4.Text != null && txt4.Visible == true)
            {
                cetri = double.Parse(txt4.Text);
            }
            switch (izb)
            {
                case "Kvadrat": Kvadrat k = new Kvadrat(jedan); izlaz = k.Povrsina();
                    break;
                case "Pravugaonik": Pravugaonik p = new Pravugaonik(jedan, dva); izlaz = p.PovrsinaPravugaonika();
                    break;
                case "Trougao": Trougao t = new Trougao(jedan, dva, tri); izlaz = t.PovrsinaTrougla();
                    break;
                case "Kocka": Kocka koc = new Kocka(jedan); izlaz = koc.Povrsina(); 
                    break;
                case "Lopta": Lopta lop = new Lopta(jedan); izlaz = lop.rKvadratPI();
                    break;
                case "Pravilna trostrana piramida": Piramida pir = new Piramida(jedan, dva); izlaz = pir.PovrsinaTrostranaZarubljena();
                    break;
                case "Pravilna cetvorostrana": Piramida pirr = new Piramida(jedan, dva); izlaz = pirr.PovrsinaCetvorostranaZarubljena();
                    break;
                case "Pravilna sestostrana piramida": Piramida pirrr = new Piramida(jedan, dva); izlaz = pirrr.PovrsinaPravilnaSestostranaPiramida();
                    break;
                case "Trostrana zarubljena piramida": Piramida trostranaZarubljena = new Piramida(jedan, dva, tri); izlaz = trostranaZarubljena.PovrsinaTrostranaZarubljena();
                    break;
                case "Cetvorostrana zarubljena piramida": Piramida cetvorostranaZarubljena = new Piramida(jedan, dva, tri); izlaz = cetvorostranaZarubljena.PovrsinaPravilnaCetvorostranaPiramida();
                    break;
                case "Zarubljena kupa": Kupa ZarubljenaKupa = new Kupa(jedan, dva, tri); izlaz = ZarubljenaKupa.PovrsinaZarubljenaKupa()
;                    break;
                case "Kupa": Kupa kupa = new Kupa(jedan, dva, tri); izlaz = kupa.PovrsinaKupa();
                    break;
                case "Valjak": Valjak valja = new Valjak(jedan, dva); izlaz = valja.PovrsinaValjka();
                    break;
                default:
                    break;
            }
            lblRezultat.Text = "Povrsina je: " + izlaz.ToString();
        }

    }
}

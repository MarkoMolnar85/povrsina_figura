﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Kupa : KrugOsnovna
    {
        double rV;     //veliko r

        public double RV
        {
           get { return rV; }
           set { rV = value; }
        }
        double s;

        public double S
        {
           get { return s; }
           set { s = value; }
        }
        public Kupa(double fr, double frv, double fs) :base(fr)
        {
            rV = frv; s = fs;
        }
        public override double rKvadratPI()
        {
            return base.rKvadratPI();
        }
        public double PovrsinaKupa()
        {
            return rKvadratPI() + R * s * Math.PI;
        }
        public double PovrsinaZarubljenaKupa()
        {
            return Math.PI * (rV * rV + R * R + s * (rV + R));
        }
    }
}

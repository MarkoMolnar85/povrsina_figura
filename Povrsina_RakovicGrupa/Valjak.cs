﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Valjak : KrugOsnovna
    {
        double h;

        public double H
        {
            get { return h; }
            set { h = value; }
        }
        public Valjak(double fr, double fh) 
            : base(fr)
        {
            h = fh;
        }
        public override double Obim()
        {
            return base.Obim();
        }
        public double PovrsinaValjka()
        {
            return Obim() * (R + h);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Kocka : Kvadrat
    {
        public Kocka(double a) 
            :base(a)
        {}
        public override double Povrsina()
        {
            return base.Povrsina() * 6;
        }
    }
}

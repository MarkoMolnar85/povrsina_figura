﻿namespace Povrsina_RakovicGrupa
{
    partial class frmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.lblPrvi = new System.Windows.Forms.Label();
            this.lblDrugi = new System.Windows.Forms.Label();
            this.lblTreci = new System.Windows.Forms.Label();
            this.cmbIzbor = new System.Windows.Forms.ComboBox();
            this.lblIzbor = new System.Windows.Forms.Label();
            this.lblCetvrti = new System.Windows.Forms.Label();
            this.txt4 = new System.Windows.Forms.TextBox();
            this.btnPovrsina = new System.Windows.Forms.Button();
            this.lblRezultat = new System.Windows.Forms.Label();
            this.imgSlike = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgSlike)).BeginInit();
            this.SuspendLayout();
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(109, 27);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(139, 20);
            this.txt1.TabIndex = 0;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(109, 53);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(139, 20);
            this.txt2.TabIndex = 0;
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(109, 79);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(139, 20);
            this.txt3.TabIndex = 0;
            // 
            // lblPrvi
            // 
            this.lblPrvi.AutoSize = true;
            this.lblPrvi.Location = new System.Drawing.Point(22, 27);
            this.lblPrvi.Name = "lblPrvi";
            this.lblPrvi.Size = new System.Drawing.Size(35, 13);
            this.lblPrvi.TabIndex = 1;
            this.lblPrvi.Text = "label1";
            // 
            // lblDrugi
            // 
            this.lblDrugi.AutoSize = true;
            this.lblDrugi.Location = new System.Drawing.Point(22, 56);
            this.lblDrugi.Name = "lblDrugi";
            this.lblDrugi.Size = new System.Drawing.Size(35, 13);
            this.lblDrugi.TabIndex = 1;
            this.lblDrugi.Text = "label1";
            // 
            // lblTreci
            // 
            this.lblTreci.AutoSize = true;
            this.lblTreci.Location = new System.Drawing.Point(22, 86);
            this.lblTreci.Name = "lblTreci";
            this.lblTreci.Size = new System.Drawing.Size(35, 13);
            this.lblTreci.TabIndex = 1;
            this.lblTreci.Text = "label1";
            // 
            // cmbIzbor
            // 
            this.cmbIzbor.FormattingEnabled = true;
            this.cmbIzbor.Location = new System.Drawing.Point(400, 27);
            this.cmbIzbor.Name = "cmbIzbor";
            this.cmbIzbor.Size = new System.Drawing.Size(115, 21);
            this.cmbIzbor.TabIndex = 2;
            this.cmbIzbor.SelectedIndexChanged += new System.EventHandler(this.cmbIzbor_SelectedIndexChanged);
            // 
            // lblIzbor
            // 
            this.lblIzbor.AutoSize = true;
            this.lblIzbor.Location = new System.Drawing.Point(352, 30);
            this.lblIzbor.Name = "lblIzbor";
            this.lblIzbor.Size = new System.Drawing.Size(30, 13);
            this.lblIzbor.TabIndex = 3;
            this.lblIzbor.Text = "Izbor";
            // 
            // lblCetvrti
            // 
            this.lblCetvrti.AutoSize = true;
            this.lblCetvrti.Location = new System.Drawing.Point(22, 115);
            this.lblCetvrti.Name = "lblCetvrti";
            this.lblCetvrti.Size = new System.Drawing.Size(35, 13);
            this.lblCetvrti.TabIndex = 4;
            this.lblCetvrti.Text = "label1";
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(109, 108);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(137, 20);
            this.txt4.TabIndex = 5;
            // 
            // btnPovrsina
            // 
            this.btnPovrsina.Location = new System.Drawing.Point(400, 231);
            this.btnPovrsina.Name = "btnPovrsina";
            this.btnPovrsina.Size = new System.Drawing.Size(134, 60);
            this.btnPovrsina.TabIndex = 6;
            this.btnPovrsina.Text = "Povrsina";
            this.btnPovrsina.UseVisualStyleBackColor = true;
            this.btnPovrsina.Click += new System.EventHandler(this.btnPovrsina_Click);
            // 
            // lblRezultat
            // 
            this.lblRezultat.AutoSize = true;
            this.lblRezultat.Location = new System.Drawing.Point(397, 315);
            this.lblRezultat.Name = "lblRezultat";
            this.lblRezultat.Size = new System.Drawing.Size(35, 13);
            this.lblRezultat.TabIndex = 7;
            this.lblRezultat.Text = "label1";
            // 
            // imgSlike
            // 
            this.imgSlike.Location = new System.Drawing.Point(49, 183);
            this.imgSlike.Name = "imgSlike";
            this.imgSlike.Size = new System.Drawing.Size(291, 220);
            this.imgSlike.TabIndex = 8;
            this.imgSlike.TabStop = false;
            // 
            // frmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 448);
            this.Controls.Add(this.imgSlike);
            this.Controls.Add(this.lblRezultat);
            this.Controls.Add(this.btnPovrsina);
            this.Controls.Add(this.txt4);
            this.Controls.Add(this.lblCetvrti);
            this.Controls.Add(this.lblIzbor);
            this.Controls.Add(this.cmbIzbor);
            this.Controls.Add(this.lblTreci);
            this.Controls.Add(this.lblDrugi);
            this.Controls.Add(this.lblPrvi);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Name = "frmGlavnaForma";
            this.Text = "Povrsina Tela";
            ((System.ComponentModel.ISupportInitialize)(this.imgSlike)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.Label lblPrvi;
        private System.Windows.Forms.Label lblDrugi;
        private System.Windows.Forms.Label lblTreci;
        private System.Windows.Forms.ComboBox cmbIzbor;
        private System.Windows.Forms.Label lblIzbor;
        private System.Windows.Forms.Label lblCetvrti;
        private System.Windows.Forms.TextBox txt4;
        private System.Windows.Forms.Button btnPovrsina;
        private System.Windows.Forms.Label lblRezultat;
        private System.Windows.Forms.PictureBox imgSlike;
    }
}


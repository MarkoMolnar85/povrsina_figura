﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class Pravugaonik //osnova za pravugaonike sve
    {
        private double a;

        public double A
        {
            get { return a; }
            set { a = value; }
        }
        double b;

        public double B
        {
            get { return b; }
            set { b = value; }
        }
        public Pravugaonik(double fa, double fb)
        {
            a = fa; b = fb;
        }
        virtual public double PovrsinaPravugaonika()
        {
            return a*b;
        }
    }
}

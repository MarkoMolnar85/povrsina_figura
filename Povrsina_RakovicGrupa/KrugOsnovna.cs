﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Povrsina_RakovicGrupa
{
    class KrugOsnovna
    {
        double r;

        public double R
        {
            get { return r; }
            set { r = value; }
        }
        public KrugOsnovna(double fr)
        {
            r = fr;
        }
        virtual public double rKvadratPI()
        {
            return r * r * Math.PI;
        }
        virtual public double Obim()
        {
            return 2 * r * Math.PI;
        }
    }
}
